package main;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		ContaLinha contaLinha = new ContaLinha();
		String arquivo = "/*teste\n"
				+ "teste*/\n"
				+ "teste";
		
		System.out.println(contaLinha.contador(arquivo));
		System.out.println(contaLinha.contLinesComent(arquivo));
		
	}

}
