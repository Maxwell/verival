package main;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ContaLinha {

	public int contador(String arquivo) throws IOException {
		String teste[] = arquivo.split("[\n|\r]");

		int cont = 0;
		while (cont < teste.length) {
			System.out.println(teste[cont]);
			cont++;
		}
		return arquivo.split("[\n|\r|]").length - contLinesComent(arquivo) ;
	}

	public int contLinesComent(String arquivo) throws IOException
	{
		InputStream is = new ByteArrayInputStream(arquivo.getBytes());
		String line="";
		
		BufferedReader br=new BufferedReader(new InputStreamReader(is));
        int count = 0;
		while((line=br.readLine())!=null)
        {
            if(line.startsWith("//"))
                count++;
            if(line.startsWith("/*"))
            {
                count++;
                
                while(!line.endsWith("*/"))
                {
                count++;
                break;
                }
               
            }

        }
        br.close();
        return count;
	}
}
