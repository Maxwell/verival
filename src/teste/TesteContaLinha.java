package teste;
import static org.junit.Assert.*;

import java.io.IOException;

import main.ContaLinha;

import org.junit.Before;
import org.junit.Test;

public class TesteContaLinha {
	ContaLinha contaLinha;
	
	@Before
	public void setup(){
		contaLinha = new ContaLinha();
	}
	@Test
	public void arquivoNaoNulo() throws IOException {
		String arquivo = "\n";
		assertEquals( 0,contaLinha.contador(arquivo));
	}
	
	@Test
	public void contaUmaLinha() throws IOException{
		String arquivo = "Uma linha";
			  	
		assertEquals(1, contaLinha.contador(arquivo));
	}
	@Test
	public void contaDuasLinhas() throws IOException{
		String arquivo = "UmaLinha\nDuasLinhas";
		
		assertEquals(2, contaLinha.contador(arquivo));
	}
	@Test
	public void naoContaLinhaBranca() throws IOException{
		String arquivo = "Linha\n"+""
				+ "testando";
		
		assertEquals(2, contaLinha.contador(arquivo));
		
	}
	@Test
	public void testaConetarioSimples() throws IOException{
		String arquivo = "//Teste\n"
						+ "teste";
	assertEquals(1,contaLinha.contador(arquivo));
	}
	@Test
	public void testaMultiplasLinhas() throws IOException{
		String arquivo = "/*teste\n"
				+ "teste*/";
		
		assertEquals(0,contaLinha.contador(arquivo));
	}
	
	
 
}
